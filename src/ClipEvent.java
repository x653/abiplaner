/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

public class ClipEvent {

    public static int EMTPY=0;
    public static int RECOPY=1;
    public static int COPY=2;
    public static int PASTE=3;
    
    private final Object wer;
    private final int was;
    
    public ClipEvent(Object wer,int waslos){
        this.wer=wer;
        this.was=waslos;
    }
    public int getWas(){
        return was;
    }
    public Object getWer(){
        return wer;
    }
}
