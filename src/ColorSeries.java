/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.awt.Color;

public class ColorSeries {
    private static Color last=null;
    private static float[] hsb=new float[3];
    
    public static Color getNext(){
        if (last==null){
            reset();
        }else {
            hsb[0]=hsb[0]+(float)7/17;
            hsb[1]=hsb[1]-(float)1/11;
            hsb[2]=hsb[2]+(float)0;
            if (hsb[0]>1)hsb[0]=hsb[0]-1;
            if (hsb[1]<(float)0.2) hsb[1]=hsb[1]+(float)0.6;            
        }
        last=Color.getHSBColor(hsb[0],hsb[1],hsb[2]);
        return last;
    }
    public static void reset(){
        hsb[0]=(float)0.575;
        hsb[1]=(float)0.8;
        hsb[2]=1;
    }
    
}
