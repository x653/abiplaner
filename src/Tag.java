/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Tag {
    
    private final String tag;
    private final boolean vn;
    private final ArrayList<BlockPruefung> bloecke;
    private final ArrayList<SlotAufsicht> aufsichten;
    private final JPanel pruef;
    private final JPanel aufsi;
    private final JPanel panel;
    private JButton jButtonaddBlock;
    
    public Tag(String tag,boolean vn) {
        this.tag=tag;
        this.vn=vn;
        bloecke = new ArrayList<>();
        aufsichten = new ArrayList<>();
        panel = new JPanel();
        pruef = new JPanel();
        aufsi = new JPanel();
        
        initComponents();
    }
    
    private void initComponents() {
        panel.setLayout(new BorderLayout());
        
        pruef.setLayout(new BoxLayout(pruef, BoxLayout.Y_AXIS));
        pruef.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        
        JPanel kopf=new JPanel();
        kopf.setLayout(new java.awt.GridLayout(1,8));
        JLabel fach=new JLabel("Fach");
        fach.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        JLabel raum= new JLabel("Raum");
        raum.setHorizontalAlignment(JLabel.CENTER);
        raum.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        kopf.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        kopf.setPreferredSize(new Dimension(1000,50));
        kopf.add(fach);
        kopf.add(raum);
        
        JLabel[] zeiten = new JLabel[6];
        for (int i=0;i<6;i++){
            zeiten[i]=new JLabel(Zeiten.getZeit(vn,i));
            zeiten[i].setHorizontalAlignment(JLabel.CENTER);
            kopf.add(zeiten[i]);   
            zeiten[i].setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        }
        
        pruef.add(kopf);
        panel.add(pruef,BorderLayout.PAGE_START);
        
        aufsi.setLayout(new BoxLayout(aufsi, BoxLayout.Y_AXIS));
        aufsi.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        
        JPanel jPanelAufsicht = new JPanel();
        jPanelAufsicht.setLayout(new java.awt.GridLayout(1,4));
        JLabel aufsichtA=new JLabel("Aufsichten");
        jPanelAufsicht.add(aufsichtA);
        JLabel aufsichtAR=new JLabel("Raum");
        aufsichtAR.setHorizontalAlignment(JLabel.CENTER);
        aufsichtAR.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanelAufsicht.add(aufsichtAR);
        JLabel aufsicht1= new JLabel(Zeiten.getAufsichtZeit(vn, 0));
        aufsicht1.setHorizontalAlignment(JLabel.CENTER);
        jPanelAufsicht.add(aufsicht1);
        JLabel aufsicht2= new JLabel(Zeiten.getAufsichtZeit(vn, 1));
        aufsicht2.setHorizontalAlignment(JLabel.CENTER);
        jPanelAufsicht.add(aufsicht2);
        jPanelAufsicht.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        aufsichtA.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        aufsicht1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        aufsicht2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        aufsi.add(jPanelAufsicht);
        jButtonaddBlock = new JButton("Neuer Prüfungsblock");
        jButtonaddBlock.setPreferredSize(new Dimension(50,50));
        panel.add(jButtonaddBlock, BorderLayout.CENTER);
        panel.add(aufsi,BorderLayout.PAGE_END);
    }
    
    public void deleteLeereBlocke(){
        ArrayList<BlockPruefung> d=new ArrayList<>();
        for (int i=0;i<bloecke.size();i++){
            if (bloecke.get(i).isEmpty()) d.add(bloecke.get(i));
        }
        for (int i=0;i<d.size();i++){
            pruef.remove(d.get(i).getPanel());
            bloecke.remove(d.get(i));
        }
        pruef.updateUI();
    }

        public void deleteLeereBlockeAufsicht(){
        ArrayList<SlotAufsicht> d=new ArrayList<>();
        for (int i=0;i<aufsichten.size();i++){
            if (aufsichten.get(i).isEmpty()) d.add(aufsichten.get(i));
        }
        for (int i=0;i<d.size();i++){
            aufsi.remove(d.get(i).getPanel());
            aufsichten.remove(d.get(i));
        }
        aufsi.updateUI();
        pruef.updateUI();
    }

    public void neuerBlockPruefung(){
        BlockPruefung p =new BlockPruefung(this);
        bloecke.add(p);
        pruef.add(p.getPanel());
        panel.updateUI();
        p.addActionListener();
    }
    
    public void addBlockPruefung(BlockPruefung p){
        bloecke.add(p);
        pruef.add(p.getPanel());
        panel.updateUI();
    }
    
    public void neuerBlockAufsicht(){
        
        SlotAufsicht p =new SlotAufsicht(this);
        aufsichten.add(p);
        aufsi.add(p.getPanel());
        panel.updateUI();
        p.addActionListener();
    }
    public void addBlockAufsicht(SlotAufsicht p){
        aufsichten.add(p);
        aufsi.add(p.getPanel());
        panel.updateUI();
    }

    public void addActionListener(){
        jButtonaddBlock.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                neuerBlockPruefung();
            }
        });

        for (int i=0;i<bloecke.size();i++){
            bloecke.get(i).addActionListener();
        }
        for (int i=0;i<aufsichten.size();i++){
            aufsichten.get(i).addActionListener();
        }
    }
    
    public ArrayList<BlockPruefung> getBloecke(){
        return bloecke;
    }
    
    public ArrayList<SlotAufsicht> getAufsichtBloecke(){
        return this.aufsichten;
    }
    public JPanel getPanel(){
         return panel;
    }
    public boolean getVN(){
        return vn;
    }
    @Override
     public String toString(){
         if (vn) return tag+" (V)";
         return tag+" (N)";
     }
    
}
