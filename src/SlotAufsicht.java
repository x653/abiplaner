/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class SlotAufsicht {
    private final JPanel jPanelAufsicht;
    private final AufsichtLehrer aufsicht1;
    private final AufsichtLehrer aufsicht2;
    private final AufsichtRaum aufraum;
    private final String name;
    private final Tag tag;
    
    public SlotAufsicht(Tag t){
        name = JOptionPane.showInputDialog("Neuer Aufsichtsraum:");
        tag=t;
        jPanelAufsicht=new JPanel();
        jPanelAufsicht.setLayout(new java.awt.GridLayout(1,4));
        JLabel aufraumV=new JLabel(name);
        aufraumV.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanelAufsicht.add(aufraumV);
        
        aufraum=new AufsichtRaum(this);
        jPanelAufsicht.add(aufraum.getView());
        
        aufsicht1=new AufsichtLehrer(this,true);
        jPanelAufsicht.add(aufsicht1.getView());
        aufsicht2=new AufsichtLehrer(this,false);
        jPanelAufsicht.add(aufsicht2.getView());
        
        
        jPanelAufsicht.setPreferredSize(new Dimension(30,30));
        jPanelAufsicht.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
    }
    public SlotAufsicht(Tag t,String name){
        tag=t;
        this.name=name;
        jPanelAufsicht=new JPanel();
        jPanelAufsicht.setLayout(new java.awt.GridLayout(1,4));
        JLabel aufraumV=new JLabel(name);
        aufraumV.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanelAufsicht.add(aufraumV);
        
        aufraum=new AufsichtRaum(this);
        jPanelAufsicht.add(aufraum.getView());
        
        aufsicht1=new AufsichtLehrer(this,true);
        jPanelAufsicht.add(aufsicht1.getView());
        aufsicht2=new AufsichtLehrer(this,false);
        jPanelAufsicht.add(aufsicht2.getView());
        
        
        jPanelAufsicht.setPreferredSize(new Dimension(30,30));
        jPanelAufsicht.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
    }
    
    public String getName(){
        return name;
    }
    public String getRaum(){
        return this.aufraum.toString();
    }
    
    public AufsichtRaum getAufsichtRaum(){
        return aufraum;
    }
    public AufsichtLehrer getAufsichtLehrer(int i){
        if (i==0) return aufsicht1;
        if (i==1) return aufsicht2;
        return null;
    }
    public Tag getTag(){return tag;}
    
    public boolean isEmpty(){
        if (!aufsicht1.isEmpty()) return false;
        if (!aufsicht2.isEmpty()) return false;
        if (!aufraum.isEmpty()) return false;
        return true;
    }
    
    public void addActionListener(){
        aufsicht1.addActionListener();
        aufsicht2.addActionListener();
        aufraum.addActionListener();
    }
   
    
    public JPanel getPanel(){
        return jPanelAufsicht;
    }
    
}
