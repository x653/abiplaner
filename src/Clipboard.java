/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.util.Observable;

public class Clipboard extends Observable {
    private Lehrer l;
    private Pruefung p;
    private Raum r;
    private static final Clipboard clip = new Clipboard();
    
    private Clipboard(){
        l=null;
        p=null;
        r=null;
    }
    
    public static Clipboard getClip(){
        return clip;
    }
    
    public void copy(Lehrer l){
        System.out.println("copy");
        removeSelection();
        this.l=l;
        r=null;
        p=null;
        l.select(true);
        this.setChanged();
        this.notifyObservers(new ClipEvent(l,ClipEvent.COPY));
    }
    
    public void removeSelection(){
        if (this.l!=null) this.l.select(false);
        if (this.p!=null) this.p.getSchueler().select(false);
        if (this.r!=null) this.r.select(false);
    }

    public void copy(Pruefung p){
        removeSelection();
        this.p=p;
        l=null;
        r=null;
        this.setChanged();
        this.notifyObservers(new ClipEvent(p,ClipEvent.COPY));
        p.getSchueler().select(true);
    }
    public void copy(Raum r){
        removeSelection();
        this.r=r;
        l=null;
        p=null;
        this.setChanged();
        this.notifyObservers(new ClipEvent(r,ClipEvent.COPY));
        r.select(true);
    }
    
    public void recopy(Pruefung p){
        removeSelection();
        this.p=p;
        r=null;
        l=null;
        this.setChanged();
        p.getSchueler().select(true);
        this.notifyObservers(new ClipEvent(p,ClipEvent.RECOPY));
    }
    
   public void pasteLehrer(){
        this.setChanged();
        this.notifyObservers(new ClipEvent(l,ClipEvent.PASTE));
    }
   
   public Lehrer getLehrer(){
        return l;
    }
    
    public Raum pasteRaum(){
        Raum rr=r;
        this.setChanged();
        this.notifyObservers(new ClipEvent(r,ClipEvent.PASTE));
        return rr;
    }

    public boolean hasPruefung(){
        return (p!=null);
    }
    public boolean hasLehrer(){
        return (l!=null);
    }
    public boolean hasRaum(){
        return (r!=null);
    }
    
    public Pruefung pastePruefung(){
        Pruefung pp=p;
        p=null;
        this.setChanged();
        this.notifyObservers(new ClipEvent(pp,ClipEvent.PASTE));
        return pp;
    }
}
