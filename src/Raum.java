/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.awt.Color;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public class Raum {
    
    private final String raum;
    private final ArrayList<Bunt> einsaetze;
    private boolean select;
    
    public Raum(String r){
        select=false;
        raum=r;
        einsaetze=new ArrayList<>();
    }
    
    public void addEinsatz(Bunt b){
        einsaetze.add(b);
        checkKollision();
    }
        
    
    public void removeEinsatz(Bunt b){
        b.update(false,false);
        einsaetze.remove(b);
        checkKollision();
    }
    public boolean isSelect(){
        return select;
    }
    public void select(boolean a){
        select=a;
        checkKollision();
    }
    
    public void checkKollision(){
        for (int i=0;i<einsaetze.size();i++){
            einsaetze.get(i).update(false,select);
        }
        for (int i=0;i<einsaetze.size();i++){
            Bunt p1=einsaetze.get(i);
            for (int j=i+1;j<einsaetze.size();j++){
                Bunt p2=einsaetze.get(j);
                    if (p1.getTag()==p2.getTag()){
                        p1.update(true,select);
                        p2.update(true,select);
                    }
            }
        }
    }
    
    @Override
    public String toString(){
        return raum;
    }
    
}