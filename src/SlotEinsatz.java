/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.awt.Color;
import java.awt.event.MouseAdapter;
import javax.swing.JLabel;


public class SlotEinsatz {
    private final JLabel label;
    private Einsatz einsatz;
    private final SlotPruefung slot;
    
    public SlotEinsatz(SlotPruefung s){
        slot = s;
        label = new JLabel();
        label.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label.setBorder(javax.swing.BorderFactory.createLineBorder(Color.BLACK,1));
    }
    public JLabel getLabel(){
        return label;
    }
    public void addActionListener(){
        
        label.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        System.out.println("clicks"+evt.getClickCount());
                        clipLehrer(evt.getClickCount());
                        System.out.println("Lehrer");
                    }
        });
        
    }
    
    public void update(boolean arg,boolean select) {
        if (arg){
            label.setOpaque(true);
            label.setBackground(Color.RED);
        } else if (select){
            label.setOpaque(true);
            label.setBackground(Color.GREEN);
        }else{ 
            label.setOpaque(false);
            label.setBackground(Color.white);
        }
    }

    public Tag getTag(){
        return slot.getTag();
    }
    
    
    protected void clipLehrer(int i){
        if (Clipboard.getClip().hasLehrer() && einsatz!=null){
            Lehrer l=Clipboard.getClip().getLehrer();
            if (i==1){
                einsatz.setLehrer(l);
            } else if (i==2){
                Pruefung[] n=slot.getNachbar();
                if (n[0]!=null) n[0].getEinsaetze()[einsatz.getTyp()].setLehrer(l);
                if (n[1]!=null) n[1].getEinsaetze()[einsatz.getTyp()].setLehrer(l);
            }
            Clipboard.getClip().pasteLehrer();
        }
    }
    
    public void setPruefungEinsatz(Einsatz p){        
        einsatz=p;
        einsatz.setLehrerSlot(this);
    }
    
    public void deletePruefungEinsatz(int t){
        if (einsatz!=null){
        einsatz.removeLehrerSlot();
        }
        einsatz=null;
    }
    
    public void update(String s) {
        label.setText(s);
    }

    
    
}
