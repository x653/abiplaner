/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
import java.awt.Color;
import java.awt.event.MouseAdapter;
import javax.swing.JLabel;


public class SlotRaum implements Bunt{
    private Raum raum;
    private final BlockPruefung block;
    private final JLabel view;
    
    public SlotRaum(BlockPruefung b){
        block = b;    
        view = new JLabel();
        view.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        view.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        //addActionListener();
    }
    
    public final void addActionListener(){
        view.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                System.out.println("Click Pruefungsraum");
                action();
            }
        });
    }
    
    public void action(){
        Clipboard c = Clipboard.getClip();
        if (this.raum==null){
            if (c.hasRaum()) setRaum(c.pasteRaum());
        } else {
            if (c.hasRaum()){
                Raum r=c.pasteRaum();
                if (raum==r) clearRaum();
                else setRaum(r);
            } else {
                c.copy(raum);
                clearRaum();
            }
        }
    }
    
    public void setRaum(Raum r){
        if (r!=null){
        if (raum!=null) clearRaum();
        raum=r;
        raum.addEinsatz(this);
        update(raum.toString());
        }
    }
    
    public void clearRaum(){
        Raum r=raum;
        raum.removeEinsatz(this);
        raum=null;
        update("");
        update(false,false);
    }

    public JLabel getView(){
        return view;
    }
    
    public void update(String arg) {
        view.setText(arg);
    }
    
    public void update(boolean arg,boolean select) {
        if (arg){
            view.setOpaque(true);
            view.setBackground(Color.RED);
        } else if (select){
            view.setOpaque(true);
            view.setBackground(Color.GREEN);
        }else{ 
            view.setOpaque(false);
            view.setBackground(Color.white);
        }
    }
    
    public Tag getTag(){return block.getTag();}
    
    public boolean hasKollision(Bunt p){
        return (this.getTag()==p.getTag());
    }
    @Override
    public String toString(){
        if (raum==null) return "";
        return raum.toString();
    }

}