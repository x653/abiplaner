/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class AbiPlaner extends javax.swing.JFrame implements Observer{  

    private final JComboBox<String> filter;
    private final JMenu jMenuAdd;
    private final JMenuItem jMenuAddTag;
    private final JMenuItem jMenuAddBlock;
    private final JMenuItem jMenuAddBlockA;
    private final JLabel jLabelClip;
    private final JList<String> jListLehrer;
    private final JList<String> jListRaum;
    private final JList<String> jListSchueler;
    private final JMenu jMenuFile;
    private final JMenu jMenuImport;
    private final JMenu jMenuExport;
    private final JMenuBar jMenuBar1;
  //  private final JMenuItem jMenuFileNew;
    private final JMenuItem jMenuFileLoadTxt;
    private final JMenuItem jMenuFileSaveTxt;
    private final JMenuItem jMenuFileSaveAs;
    private final JMenuItem jMenuFileQuit;
    private final JMenuItem jMenuImportLehrer;
    private final JMenuItem jMenuImportSchueler;
    private final JMenuItem jMenuImportSchueler3;
    private final JMenuItem jMenuImportRaum;
    private final JSplitPane jSplitPane1;
    private final JTabbedPane jTabbedPaneTage;
    private final JTabbedPane jTabPaneAuswahl;
    
    private Model model;
    private final JMenuItem jMenuExportPDF;
    private final JMenuItem jMenuAddDelBlock;
    private final JMenuItem jMenuAddRaum;
    private final JMenuItem jMenuAddLehrer;
    private final JMenuItem jMenuAddSchueler;
    private File datei;
    /**
     * Creates new form NewJFrame
     */
    public AbiPlaner() {
        model = new Model();
        jListSchueler = new javax.swing.JList<>();
        jLabelClip = new javax.swing.JLabel("leer");
        jSplitPane1 = new javax.swing.JSplitPane();
        jTabbedPaneTage = new javax.swing.JTabbedPane();
        jTabPaneAuswahl = new javax.swing.JTabbedPane();
        jListLehrer = new javax.swing.JList<>();
        jListRaum = new javax.swing.JList<>();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenuFile = new javax.swing.JMenu();
    //    jMenuFileNew = new javax.swing.JMenuItem();
        jMenuFileLoadTxt = new javax.swing.JMenuItem();
        jMenuFileSaveTxt = new javax.swing.JMenuItem();
        jMenuFileSaveAs = new javax.swing.JMenuItem();
        jMenuFileQuit = new javax.swing.JMenuItem();
        jMenuImport = new javax.swing.JMenu();
        jMenuImportLehrer = new javax.swing.JMenuItem();
        jMenuImportSchueler = new javax.swing.JMenuItem();
        jMenuImportSchueler3 = new javax.swing.JMenuItem();
        jMenuImportRaum = new javax.swing.JMenuItem();
        jMenuExport = new javax.swing.JMenu();
        jMenuExportPDF = new javax.swing.JMenuItem();
        jMenuAdd=new JMenu();
        jMenuAddTag=new JMenuItem();
        jMenuAddBlock=new JMenuItem();
        jMenuAddBlockA=new JMenuItem();
        jMenuAddDelBlock=new JMenuItem();
        jMenuAddRaum = new JMenuItem();
        jMenuAddLehrer = new JMenuItem();
        jMenuAddSchueler = new JMenuItem();
        filter=new JComboBox<>();
        
        initComponent();
    }
    
    private void initComponent(){
        Clipboard.getClip().addObserver(this);
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        getContentPane().add(jLabelClip, BorderLayout.PAGE_END);
        jTabbedPaneTage.setPreferredSize(new Dimension(1000,400));
        jSplitPane1.setRightComponent(jTabbedPaneTage);
        jTabPaneAuswahl.addTab("SuS", new JScrollPane(jListSchueler));
        jTabPaneAuswahl.addTab("LuL", new JScrollPane(jListLehrer));


        jTabPaneAuswahl.addTab("R", new JScrollPane(jListRaum));
        JPanel links=new JPanel();
        links.setLayout(new BorderLayout());
        links.add(jTabPaneAuswahl, BorderLayout.CENTER);
        
        links.add(filter,BorderLayout.PAGE_START);
        
        jSplitPane1.setLeftComponent(links);
        
        jTabPaneAuswahl.setPreferredSize(new Dimension(200,400));
        getContentPane().add(jSplitPane1, BorderLayout.CENTER);
        jTabPaneAuswahl.addChangeListener((ChangeEvent e) -> {
            switch(jTabPaneAuswahl.getSelectedIndex()){
                case 2:
                    jListRaumValueChanged(null);
                    break;
                case 1:
                    jListLehrerValueChanged(null);
                    break;
                case 0:
                    jListSchuelerValueChanged(null);
                    break;
            }
        });
        
        jMenuFile.setText("File");
        
//        jMenuFileNew.setText("New");
//        jMenuFile.add(jMenuFileNew);
//        jMenuFileNew.addActionListener(this::jMenuItemFileNew);

        jMenuFileLoadTxt.setText("Load");
        jMenuFile.add(jMenuFileLoadTxt);
        jMenuFileLoadTxt.addActionListener(this::jMenuItemFileLoadTxt);

        jMenuFileSaveTxt.setText("Save");
        jMenuFileSaveTxt.addActionListener(this::jMenuItemSaveTxt);
        jMenuFile.add(jMenuFileSaveTxt);
        
        jMenuFileSaveAs.setText("Save As");
        jMenuFileSaveAs.addActionListener(this::jMenuItemSaveAs);
        jMenuFile.add(jMenuFileSaveAs);
        
        jMenuFileQuit.setText("Quit");
        jMenuFileQuit.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] options = { "JA", "NEIN","ZURÜCK" };
                int res= JOptionPane.showOptionDialog(null, "Hey Alter, willst Du noch speichern?", "Warning",
                            JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
                                null, options, options[0]);
                System.out.println(res);
                if (res==0){
                    if (datei!=null){
                            FileIO.save(model,datei);   
                            setVisible(false); //you can't see me!
                            dispose();
                    } else {
                        chooseFolder();
                        if (!datei.exists()){
                            datei.mkdir();
                            FileIO.save(model,datei);
                        }else {
                            System.out.println(datei +" existiert schon");
                        }
                    }
                }else if (res==1){
                    
                setVisible(false); //you can't see me!
                dispose();
             }}
        }
            );
        jMenuFile.add(jMenuFileQuit);
        jMenuBar1.add(jMenuFile);
        
        jMenuAdd.setText("Bearbeite");
        jMenuAddTag.setText("neuer Tag");
        jMenuAdd.add(jMenuAddTag);
        jMenuAddTag.addActionListener((ActionEvent e) -> {
            model.addTag(jTabbedPaneTage);
        });
        jMenuAddBlock.setText("neuer Block");
        jMenuAdd.add(jMenuAddBlock);
        jMenuAddBlock.addActionListener((ActionEvent e) -> {
            model.addBlock(jTabbedPaneTage.getSelectedIndex());
        });
        jMenuAddBlockA.setText("neue Aufsicht");
        jMenuAdd.add(jMenuAddBlockA);
        jMenuAddBlockA.addActionListener((ActionEvent e) -> {
            model.addBlockA(jTabbedPaneTage.getSelectedIndex());
        });
        jMenuAddDelBlock.setText("lösche leere Blöcke");
        jMenuAdd.add(jMenuAddDelBlock);
        jMenuAddDelBlock.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.loescheLeereBloecke(jTabbedPaneTage.getSelectedIndex());
            }
        });
        jMenuAddRaum.setText("neuer Raum");
        jMenuAdd.add(jMenuAddRaum);
        jMenuAddRaum.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.addRaum();
            }
        });
        jMenuAddLehrer.setText("neuer Lehrer");
        jMenuAdd.add(jMenuAddLehrer);
        jMenuAddLehrer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.addLoL();
            }
        });
        jMenuAddSchueler.setText("neuer Schueler");
        jMenuAdd.add(jMenuAddSchueler);
        jMenuAddSchueler.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.addSoS();
            }
        });
        
        jMenuBar1.add(jMenuAdd);

        
        jMenuImport.setText("Import");
        jMenuImportLehrer.setText("Lehrer");
        jMenuImport.add(jMenuImportLehrer);
        jMenuImportSchueler.setText("Abi4");
        jMenuImport.add(jMenuImportSchueler);
        jMenuImportSchueler3.setText("Abi1-3");
        jMenuImport.add(jMenuImportSchueler3);
        jMenuImportRaum.setText("Räume");
        jMenuImport.add(jMenuImportRaum);
        jMenuBar1.add(jMenuImport);
        jMenuExport.setText("Export");
        ArrayList<String> list=new ArrayList<>();

        try {
            String[] files;
            files = this.getResourceListing(getClass(), "tex/");
            for (String f:files) {
                System.out.println(f);
                if (f.endsWith(".tex")) list.add(f);
            }

        } catch (URISyntaxException ex) {
            Logger.getLogger(AbiPlaner.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AbiPlaner.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i=0;i<list.size();i++){
            System.out.println(list.get(i));
                JMenuItem menuItem = new javax.swing.JMenuItem();
                menuItem.setText(list.get(i).substring(0,list.get(i).length()-4));
                jMenuExport.add(menuItem);
                menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                if (datei!=null){
                    File f=new File(datei+"/"+menuItem.getText()+".tex");
                    if (!f.exists()){
                        copyFile(menuItem.getText());
                    }
                    model.exportierePDF(datei,menuItem.getText());                }
                }
             });
        }
        
        jMenuBar1.add(jMenuExport);
        setJMenuBar(jMenuBar1);
        pack();
        
        jMenuImportLehrer.addActionListener((ActionEvent e) -> {
            model.getLehrer().doImport();
        });
        jMenuImportRaum.addActionListener((ActionEvent e) -> {
            model.getRaeume().doImport();
        });
        jMenuImportSchueler.addActionListener((ActionEvent e) -> {
            model.doImport(datei);
        });
        jMenuImportSchueler3.addActionListener((ActionEvent e) -> {
            model.doImport3(datei);
        });
        
        //lehrer.doImport(new File("/home/micha/x653/ABI4/lehrer.TXT"));
        //schueler.doImport(lehrer, faecher,new File("/home/micha/x653/ABI4/abi4.TXT"));
        addActionListener();
    }                   

    private void jListLehrerValueChanged(javax.swing.event.ListSelectionEvent evt) {                                    
        // TODO add your handling code here:
        int i=jListLehrer.getSelectedIndex();
        if (i>=0) {
            Lehrer l=model.getLehrer().elementAt(i);
            Clipboard.getClip().copy(l);            
        }
    }                                   
    private void jListSchuelerValueChanged(javax.swing.event.ListSelectionEvent evt) {                                    
        // TODO add your handling code here:
        int i=jListSchueler.getSelectedIndex();
        if (i>=0){
            System.out.println(model.getSchueler().elementAt(i).toString());
            Clipboard.getClip().copy(model.getSchueler().elementAt(i));
        }
    }                        
    
    private void jListRaumValueChanged(javax.swing.event.ListSelectionEvent evt) {                                    
        // TODO add your handling code here:
        int i=jListRaum.getSelectedIndex();
        if (i>=0){
            System.out.println(jListRaum.getSelectedValue());
            Clipboard.getClip().copy(model.getRaeume().elementAt(jListRaum.getSelectedIndex()));
        }
    }      
    
    
        private void jMenuItemFileLoadTxt(java.awt.event.ActionEvent evt) {   
            chooseFolder();
            if (datei!=null){
            
        // TODO add your handling code here:
        model = FileIO.load(datei);
        jTabbedPaneTage.removeAll();
        
        for (int i=0;i<model.getTage().size();i++){
            jTabbedPaneTage.add(new JScrollPane(model.getTage().get(i).getPanel()),model.getTage().get(i).toString());
            model.getTage().get(i).addActionListener();
            
        }
        addActionListener();
        }
    }
        
     
    public void copyFile(String filename){        
        InputStream file = getClass().getResourceAsStream("tex/"+filename+".tex");
            try {
              FileOutputStream fout = new FileOutputStream(datei+"/"+filename+".tex");
              PrintWriter p=new PrintWriter(fout);
                try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(file));) {
            String line;
            while ((line = reader.readLine()) != null) {
               p.println(line);
            }

        }   catch (FileNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }
            p.close();
         }
            catch (Exception e) { e.printStackTrace(); }

    }
    
    private void jMenuItemSaveAs(java.awt.event.ActionEvent evt) {  
        chooseFolder();
        if (!datei.exists()){
            datei.mkdir();
            FileIO.save(model,datei);
            copyFile("plan");
        } else {
            JOptionPane.showMessageDialog(this, "Hey Alter, "+datei.getName()+" existiert schon!");
        }
    }   
    
    
    private void jMenuItemSaveTxt(java.awt.event.ActionEvent evt) {  
        if (datei==null) chooseFolder();
        if (datei!=null) FileIO.save(model,datei);
    }                                          

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Create and display the form */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AbiPlaner.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AbiPlaner.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AbiPlaner.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AbiPlaner.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        java.awt.EventQueue.invokeLater(() -> {
            new AbiPlaner().setVisible(true);
        });
    }
    
    
    
    @Override
    public void update(Observable o, Object arg) {
        if (o==Clipboard.getClip()) {
            Object wer=((ClipEvent)arg).getWer();
            int was=((ClipEvent)arg).getWas();
            if (was==ClipEvent.EMTPY){
                if (wer==null) {
                    jLabelClip.setText("leer");
                }
            }
            else if (was==ClipEvent.PASTE){
                if (wer.getClass()==Lehrer.class) {
                    System.out.println( "lllllll"+model.getLehrer().getListDataListeners().length);
                    
//                    model.getLehrer().getListDataListeners()[0].contentsChanged(
  //                          new ListDataEvent(model.getLehrer(),
    //                                ListDataEvent.CONTENTS_CHANGED,
      //                              jListLehrer.getSelectedIndex(),
        //                            jListLehrer.getSelectedIndex()));
                    jListLehrer.updateUI();
                }else if (wer.getClass()==Pruefung.class) {
                    int i=jListSchueler.getSelectedIndex();
                    System.out.println(i);
                    model.getSchueler().removeElement(wer);
                    ((Pruefung)wer).getSchueler().select(false);
                    jListSchueler.setSelectedIndex(i);
                    
                }else if (wer.getClass()==Raum.class) {
                    int i=jListRaum.getSelectedIndex();
                    jListRaum.setSelectedIndex(i+1);
                }
            }else if (was==ClipEvent.RECOPY){
                if (wer.getClass()==Pruefung.class) {
                    jLabelClip.setText("Pruefung: "+((Pruefung)wer).toString());
                    model.getSchueler().addElement((Pruefung)wer);
                    jListSchueler.setSelectedIndex(model.getSchueler().getSize()-1);
                }
            }
            System.out.println("Observed wer: "+wer);
            System.out.println("Observed was: "+was);
                if (wer.getClass()==Lehrer.class) jLabelClip.setText("Lehrer: "+wer.toString());
                else if (wer.getClass()==Pruefung.class) jLabelClip.setText("Pruefung: "+wer.toString());
                else if (wer.getClass()==Raum.class) jLabelClip.setText("Raum: "+wer);
        }
    }
    private void addActionListener(){        
        jListLehrer.setModel(new JListModelGeil<Lehrer>(model.getLehrer()));
        while (jListLehrer.getListSelectionListeners().length>0)
            jListLehrer.removeListSelectionListener(jListLehrer.getListSelectionListeners()[0]);
        jListLehrer.addListSelectionListener(this::jListLehrerValueChanged);
        jListSchueler.setModel(new JListModelGeil<Pruefung>(model.getSchueler()));
        while (jListSchueler.getListSelectionListeners().length>0)
            jListSchueler.removeListSelectionListener(jListSchueler.getListSelectionListeners()[0]);
        jListSchueler.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                jListSchuelerValueChanged(e);
            }
        });
        jListRaum.setModel(new JListModelGeil<Raum>(model.getRaeume()));
        while (jListRaum.getListSelectionListeners().length>0)
            jListRaum.removeListSelectionListener(jListRaum.getListSelectionListeners()[0]);
        jListRaum.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                jListRaumValueChanged(e);
            }
        });
        filter.setModel(model.getFaecher());
        while (filter.getActionListeners().length>0)
            filter.removeActionListener(filter.getActionListeners()[0]);
        filter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    model.filter();
            }
        });

    }
    
    public void chooseFolder() {            
    JFileChooser chooser = new JFileChooser(); 
    chooser.setCurrentDirectory(new java.io.File("."));
    chooser.setDialogTitle("Select Folder");
    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    //
    // disable the "All files" option.
    //
    chooser.setAcceptAllFileFilterUsed(false);
    //    
    if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
        datei=chooser.getSelectedFile();
      System.out.println("getCurrentDirectory(): " 
         +  chooser.getCurrentDirectory());
      System.out.println("getSelectedFile() : " 
         +  chooser.getSelectedFile());
      }
    else {
      System.out.println("No Selection ");
      }
     }

    public void  jMenuItemFileNew(java.awt.event.ActionEvent evt){
        InputStream file = getClass().getResourceAsStream("plan.tex");
//        File file=new File(getClass().getClassLoader().getResource("plan.tex").getFile());
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(file));) {
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

        }   catch (FileNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    String[] getResourceListing(Class clazz, String path) throws URISyntaxException, IOException {
      URL dirURL = clazz.getClassLoader().getResource(path);
      if (dirURL != null && dirURL.getProtocol().equals("file")) {
        /* A file path: easy enough */
        return new File(dirURL.toURI()).list();
      } 

      if (dirURL == null) {
        /* 
         * In case of a jar file, we can't actually find a directory.
         * Have to assume the same jar as clazz.
         */
        String me = clazz.getName().replace(".", "/")+".class";
        dirURL = clazz.getClassLoader().getResource(me);
      }
      
      if (dirURL.getProtocol().equals("jar")) {
        /* A JAR path */
        String jarPath = dirURL.getPath().substring(5, dirURL.getPath().indexOf("!")); //strip out only the JAR file
        JarFile jar = new JarFile(URLDecoder.decode(jarPath, "UTF-8"));
        Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
        Set<String> result = new HashSet<String>(); //avoid duplicates in case it is a subdirectory
        while(entries.hasMoreElements()) {
          String name = entries.nextElement().getName();
          if (name.startsWith(path)) { //filter according to the path
            String entry = name.substring(path.length());
            int checkSubdir = entry.indexOf("/");
            if (checkSubdir >= 0) {
              // if it is a subdirectory, we just return the directory name
              entry = entry.substring(0, checkSubdir);
            }
            result.add(entry);
          }
        }
        return result.toArray(new String[result.size()]);
      } 
        
      throw new UnsupportedOperationException("Cannot list files for URL "+dirURL);
  }
}