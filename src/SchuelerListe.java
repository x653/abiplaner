/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;

public class SchuelerListe extends DefaultListModel<Pruefung> {

   private final ArrayList<Pruefung> filter;
   private final ArrayList<Schueler> schueler;
    
   public SchuelerListe(){
       super();
       filter=new ArrayList<>();
       schueler=new ArrayList<>();
    }
    
   public Schueler finde(String vorname,String name){
       for (int i=0;i<schueler.size();i++){
           if ((vorname.equals(schueler.get(i).getVorname())
                   &&(name.equals(schueler.get(i).getNachname()))))
               return schueler.get(i);
       }
       Schueler s=new Schueler(vorname,name);
       schueler.add(s);
       return s;
   }
   
   public void filter(String fach){
       for (int i=0;i<filter.size();i++){
           this.addElement(filter.get(i));
       }
       filter.clear();
       if (!fach.equals("ALLE")){
            for (int i=0;i<this.size();i++){
                if (!this.getElementAt(i).getFach().equals(fach)) filter.add(this.getElementAt(i));
            }
            for (int i=0;i<filter.size();i++){
                this.removeElement(filter.get(i));
            }
       }
   }

    
}
