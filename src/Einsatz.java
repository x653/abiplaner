/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.awt.Color;
import java.io.Serializable;

public class Einsatz implements Bunt{
    public static int VORSITZ=0;
    public static int PRUEFER=1;
    public static int PROTOKOLL=2;
    
    
    private final Pruefung pruefung;
    private Lehrer lehrer;
    private final int typ;
    private SlotEinsatz slot;
    
    public Einsatz(Pruefung p,int t){
        pruefung=p;
        typ=t;
    }
    
    public int getTyp(){
        return typ;
    }
    
    public void setLehrer(Lehrer l){
        if (l!=null){
        if (l==lehrer){
            lehrer.removePruefung(this);
            lehrer=null;
            slot.update(this.toString());
            slot.update(false,false);
        } else {
            if (lehrer!=null){
                lehrer.removePruefung(this);
            }
            lehrer=l;
            lehrer.addPruefung(this);
            update(this.toString());
        }
        }
    }
    
    public void setLehrerSlot(SlotEinsatz s){
        slot=s;
        slot.update(this.toString());
        if (this.lehrer!=null) lehrer.checkKollision();
    }
    
    public void removeLehrerSlot(){
        slot.update("");
        slot.update(false,false);
        slot=null;
        if (this.lehrer!=null) lehrer.checkKollision();
    }
    
    public boolean isVor(){
        return (pruefung.getSlot().getPos()/3==0);
    }
    public boolean hasKollision(Bunt p){
        if (p.getClass()==this.getClass()){
            Einsatz p2=(Einsatz)p;
        if (this.pruefung==p2.pruefung) return true;
        if ((pruefung.getSlot()!=null) && (p2.pruefung.getSlot()!=null)&&
            (pruefung.getSlot().getBlock().getTag()==p2.pruefung.getSlot().getBlock().getTag())
            && (pruefung.getSlot().getBlock()!=p2.pruefung.getSlot().getBlock())
            && (pruefung.getSlot().getPos()/3==p2.pruefung.getSlot().getPos()/3)   ) return true;
        return false;
        }else if (p.getClass()==AufsichtLehrer.class){
            AufsichtLehrer p2=(AufsichtLehrer)p;
            if ((this.getTag()==p2.getTag())&&(p2.isVor()==this.isVor())) return true;
            System.out.println("ok");
        }
        return false;
    }
    
    public void update(boolean h,boolean s){
        if (slot!=null) slot.update(h,s);
    }
    
    
    public void update(String h){
        if (slot!=null) slot.update(h);
    }
    
    @Override
    public String toString(){
        if (typ==Einsatz.PRUEFER){
            if (lehrer==null)
                if (this.pruefung.getKurslehrer()!=null)
                return "("+this.pruefung.getKurslehrer().getKuerzel()+")";
                else return "()";
            else if (lehrer==pruefung.getKurslehrer()) return lehrer.getKuerzel();
            return lehrer.getKuerzel()+"("+this.pruefung.getKurslehrer().getKuerzel()+")";
        }
        if (lehrer==null) {
            return "";
        }
        return lehrer.getKuerzel();
    }

    public Lehrer getLehrer(){
        return lehrer;
    }

    @Override
    public Tag getTag() {
        if (slot==null)return null;
        return slot.getTag();
    }
}