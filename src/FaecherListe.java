/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.util.Collections;
import javax.swing.DefaultComboBoxModel;


public class FaecherListe extends DefaultComboBoxModel<String>{
    public FaecherListe(){
        super();
    }
    
    @Override
    public void addElement(String fach){
        int i=0;
        for (i=0;i<getSize();i++){
            if (getElementAt(i).equals(fach)) return;
            if (getElementAt(i).compareTo(fach)>0) break;
        }
        super.insertElementAt(fach, i);
    }                    
}
