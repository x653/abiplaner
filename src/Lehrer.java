/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.awt.Color;
import java.util.ArrayList;


public class Lehrer implements Comparable<Lehrer>{
    private final String kuerzel;
    private final String vorname;
    private final String nachname;
    private final ArrayList<String> faecher;
    private final ArrayList<Bunt> pruefungen;
    private boolean select;
    
    public Lehrer(String pKuerzel,String pVorname, String pNachname){
        kuerzel=pKuerzel;
        vorname=pVorname;
        nachname=pNachname;
        faecher = new ArrayList<>();
        pruefungen = new ArrayList<>();
    }
    
    public int getEinsaetze(){
        return pruefungen.size();
    }
    
    public void addFach(String pFach){
        faecher.add(pFach);
    }
    
    public boolean hasFach(String pFach){
        return faecher.contains(pFach);
    }
    
    public void addPruefung(Bunt p){        
        pruefungen.add(p);
        checkKollision();
    }

    public void removePruefung(Bunt p){
        pruefungen.remove(p);
        p.update(false,false);
        checkKollision();
    }
    public boolean isSelect(){
        return select;
    }
    
    public void select(boolean s){
        this.select=s;
        this.checkKollision();
    }
    
    public void checkKollision(){
        for (int i=0;i<pruefungen.size();i++){
            pruefungen.get(i).update(false,select);
        }
        for (int i=0;i<this.pruefungen.size();i++){
            Bunt p1=pruefungen.get(i);
            for (int j=i+1;j<pruefungen.size();j++){
                Bunt p2=pruefungen.get(j);
                if (p1.hasKollision(p2)) {
                    p1.update(true,select);
                    p2.update(true,select);
                }
            }
        }
    }
    
    @Override
    public String toString(){
        StringBuilder s= new StringBuilder();
        s.append(kuerzel);
                    s.append(" (");

        if (!faecher.isEmpty()){
            for (int i=0;i<faecher.size();i++){
                s.append(faecher.get(i));
                if (i<faecher.size()-1) s.append(",");
            }
        }
        s.append(")");

        s.append(" [");
        s.append(getEinsaetze());
        s.append("]");
        return s.toString();
    }
    
    public String getKuerzel(){
        return kuerzel;
    }
    
    @Override
    public int compareTo(Lehrer o) {
         return getKuerzel().compareTo(o.getKuerzel());
    }
}
