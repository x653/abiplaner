/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class LaTeX {

    public static void doExport(Lehrer l,PrintWriter pr){
        pr.print("{");
        if (l!=null) pr.print(l.getKuerzel());
        pr.print("}");
    }
    
    public static void doExport(Schueler s,PrintWriter pr){
        pr.print("{");
        pr.print(s.getVorname());
        pr.print("\\\\");
        pr.print(s.getNachname());
        pr.print("}");
    }
    
    public static void doExport(Pruefung p,PrintWriter pr){
        if (p==null){
            pr.print("\\pruefungleer{}");
        } else {
            if (p.getSchueler().getColor()==null){
                pr.print("\\pruefung{");
            } else {
                pr.print("\\pruefungcolor{");
                pr.print(p.getSchueler().getColor().getRed());
                pr.print("}{");
                pr.print(p.getSchueler().getColor().getGreen());
                pr.print("}{");
                pr.print(p.getSchueler().getColor().getBlue());
                pr.print("}{");                
            }
            pr.print(p.getEinsaetze()[0].toString());
            pr.print("}{");
            pr.print(p.getEinsaetze()[1].toString());
            pr.print("}{");
            pr.print(p.getEinsaetze()[2].toString());
            pr.print("}");
            doExport(p.getSchueler(),pr);
        }
    }
    
    public static void doExport(String p,PrintWriter pr){
        if (p==null){
            pr.print("\\pruefungheader{}");
        } else {
            pr.print("\\pruefungheader{");
            pr.print(p);
            pr.print("}");
        }
    }

    public static void doExport(BlockPruefung b,PrintWriter pr) {
        //if (!b.isEmpty()){
            SlotPruefung[] pruef = b.getPruefungen();
            pr.print("\\fachraum{");
            pr.print(b.getFach());
            pr.print("}{");
            pr.print(b.getRaum().toString());
            pr.print("}&");
            for (int i=0;i<6;i++){
                doExport(pruef[i].getPruefung(),pr);
                if (i==5){
                    pr.println("\\\\\\hline");
                } else {
                    pr.println("&");            
                }
            }
            pruef[0].getPruefung();
        //}
    }

    public static void doExport(SlotAufsicht b,PrintWriter pr) {
            pr.print(b.getName());
            pr.print("&");
            pr.print(b.getRaum());
            pr.print("&");
            pr.print(b.getAufsichtLehrer(0).toString());
            pr.print("&");
            pr.print(b.getAufsichtLehrer(1).toString());
            pr.println("\\\\\\hline");
            
    }
    
    public static void doExport(Model p,File  f){
        PrintWriter pr = null;
        try {
            pr = new PrintWriter(f); //"UTF-8"
            doExport(p,pr);
            pr.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LaTeX.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            pr.close();
        }
        
    
    }
    
    
    public static void doExport(Tag t,PrintWriter pr){
        if (!t.getBloecke().isEmpty()){
        pr.println("\\section*{"+t.toString()+"}");
        pr.println("\\begin{longtable}[l]{|lr||c|c|c||c|c|c|}");
        pr.println("\\hline");
        pr.print("\\fachraum{Fach}{Raum}&");
        for (int i=0;i<6;i++){
            doExport(Zeiten.getZeit(t.getVN(),i),pr);
            if (i<5) pr.print("&");
            else pr.println("\\\\");
        }
        pr.println("\\hline\\hline\\endhead");
        for (int i=0;i<t.getBloecke().size();i++){
            doExport(t.getBloecke().get(i),pr);
        }
        pr.println("\\end{longtable}");
        
        pr.println("\\vspace{1cm}");
        pr.println("\\mycomment{");
        pr.println("\\subsection*{Aufsichten}");
        pr.println("\\begin{longtable}[l]{|l|c|c|c|}");
        pr.println("\\hline");
        pr.print("Aufsichten&Raum&");
        for (int i=0;i<2;i++){
            doExport(Zeiten.getAufsichtZeit(t.getVN(),i),pr);
            if (i<1) pr.print("&");
            else pr.println("\\\\");
        }
        pr.println("\\hline\\hline\\endhead");
        for (int i=0;i<t.getAufsichtBloecke().size();i++){
            doExport(t.getAufsichtBloecke().get(i),pr);
        }
        pr.println("\\end{longtable}");
        pr.println("}");
        
        }
    }
    
    public static void doExport(Model p,PrintWriter pr) {
        for (int i=0;i<p.getTage().size();i++){
            doExport(p.getTage().get(i),pr);
            pr.println("\\newpage");
        }
    }
    
}
