/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.io.Serializable;
import javax.swing.DefaultListModel;
import javax.swing.event.ListDataListener;


public class JListModelGeil<E> implements javax.swing.ListModel<String>{


    private final DefaultListModel<E> m;
    
    public JListModelGeil(DefaultListModel<E> mm){
        m=mm;
    }
   
    @Override
    public int getSize() {
        return m.getSize();
    }

    @Override
    public String getElementAt(int index) {
        return m.getElementAt(index).toString();
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        m.addListDataListener(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        m.removeListDataListener(l);
    }
}