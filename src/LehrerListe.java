/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;


public class LehrerListe extends DefaultListModel<Lehrer> {

    private final ArrayList<Lehrer> filter;
    
    public LehrerListe(){
        super();
        filter = new ArrayList<>();
    }
    
    public void filter(String fach){
        for (int i=0;i<this.size();i++){
           filter.add(this.getElementAt(i));
        }
        Collections.sort(filter);
        this.clear();
        for (int i=0;i<filter.size();i++){
                if (fach.equals("ALLE") || filter.get(i).hasFach(fach)) this.addElement(filter.get(i));
        }
        for (int i=0;i<this.size();i++){
            filter.remove(this.getElementAt(i));
        }
    }

    public Lehrer finde(String k){
        for (int i=0;i<size();i++){
            if (get(i).getKuerzel().equals(k)) return get(i);
        }
        return null;
    }
    
    public void doImport(File selectedFile){
        try (BufferedReader br = new BufferedReader(new FileReader(selectedFile))) {
            clear();
                String line;
                while ((line = br.readLine()) != null) {
                String[] s=line.split(";");
                for (int i=0;i<s.length;i++){
                    s[i]=s[i].trim();
                }
                System.out.println(line);
                if (s.length>=3 && !s[0].contains("Kürzel")){      
                    Lehrer ll=new Lehrer(s[0],s[1],s[2]);
                    addElement(ll);
                    if (s.length==4){
                    String[] f=s[3].split(",");
                        for (String f1 : f) {
                            ll.addFach(f1.trim());
                        }
                    }
                }
            }
        }   catch (FileNotFoundException ex) {
                Logger.getLogger(LehrerListe.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(LehrerListe.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    
    public void doImport() {
        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        int returnValue = jfc.showOpenDialog(null);
	if (returnValue == JFileChooser.APPROVE_OPTION) {
            File selectedFile = jfc.getSelectedFile();
            System.out.println(selectedFile.getAbsolutePath());
            doImport(selectedFile);
        }
    }
    
}
