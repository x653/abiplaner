/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
public class Zeiten {

    public final boolean VORMITTAG=true;
    public final boolean NACHMITAG=false;
    private final String[] zeitenV;
    private final String[] zeitenN;
    private final String[] zeitenAV;
    private final String[] zeitenAN;
    private static final Zeiten Z = new Zeiten();
    
    private Zeiten(){
        zeitenV=new String[6];
        zeitenV[0]="8:30 - 9:00";
        zeitenV[1]="9:00 - 9:30";
        zeitenV[2]="9:30 - 10:00";
        zeitenV[3]="11:00 - 11:30";
        zeitenV[4]="11:30 - 12:00";
        zeitenV[5]="12:00 - 12:30";
        zeitenN=new String[6];
        zeitenN[0]="14:00 - 14:30";
        zeitenN[1]="14:30 - 15:00";
        zeitenN[2]="15:00 - 15:30";
        zeitenN[3]="16:30 - 17:00";
        zeitenN[4]="17:00 - 17:30";
        zeitenN[5]="17:30 - 18:00";
        zeitenAV=new String[2];
        zeitenAV[0]="8:00 - 9:30";
        zeitenAV[1]="10:30 - 12:00";
        zeitenAN=new String[2];
        zeitenAN[0]="13:30 - 15:00";
        zeitenAN[1]="16:00 - 17:30";
        
    }
    
    public static String getZeit(boolean vn,int i){
        if (i>=0 && i<6) {
            if(vn) return Z.zeitenV[i];
            return Z.zeitenN[i];
        }
        return null;
    }
     public static String getAufsichtZeit(boolean vn,int i){
        if (i>=0 && i<2) {
            if(vn) return Z.zeitenAV[i];
            return Z.zeitenAN[i];
        }
        return null;
    }
    
}