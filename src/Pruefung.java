/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

public class Pruefung implements Bunt{
    private final Schueler schueler;
    private final String fach;
    private final Einsatz[] einsaetze;
    private Lehrer kurslehrer;
    private SlotPruefung slot;

    public Pruefung(Schueler s, String f){
        einsaetze=new Einsatz[3];
        for (int i=0;i<3;i++){
            einsaetze[i]=new Einsatz(this,i);
        }
        schueler = s;
        s.setAbi4(this);
        fach=f;
    }
    public Einsatz[] getEinsaetze(){
        return einsaetze;
    }
    public SlotPruefung getSlot(){
        return slot;
    }
    public void setSlot(SlotPruefung s){
        slot=s;
        schueler.checkKollision();
    }
    public void deleteSlot(){        
        SlotPruefung s=slot;
        slot=null;
        s.updatexx(false, false);
        schueler.checkKollision();
    }
    
    public Schueler getSchueler(){
        return schueler;
    }
    public String getFach(){
        return fach;
    }
    
    public Lehrer getKurslehrer(){
        return this.kurslehrer;
    }
    
    public void setKurslehrer(Lehrer l){
        kurslehrer=l;
        einsaetze[1].setLehrer(l);
    }
    
    @Override
    public String toString(){
        if (fach!=null && kurslehrer!=null) return fach+"("+kurslehrer.getKuerzel()+")"+ "-"+schueler.toString();
        return fach+ "-"+schueler.toString();
    }

    @Override
    public void update(boolean koll, boolean c) {
        if (slot!=null)
            slot.updatexx(koll,c);
    }

    @Override
    public Tag getTag() {
        if (slot!=null)
        return slot.getTag();
        return null;
    }
    public int isVor(){
        if (slot==null) return 0;
        return ((getSlot().getPos()/3)+1);
    }
    @Override
    public boolean hasKollision(Bunt p) {
        Pruefung p2=(Pruefung)p;
        System.out.println(p2.isVor());
        System.out.println(this.isVor());
        if (getTag()==null) return false;
        if (p2.getTag()==null) return false;
        if ((p2.getTag()==this.getTag() )&& (p2.isVor()==this.isVor())) return true;
        return false;
    }
    public void checkKollision(){
        for (Einsatz e:einsaetze) if (e.getLehrer()!=null)e.getLehrer().checkKollision();
    }
    
    
}
