/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;


public class RaumListe extends DefaultListModel<Raum> {

    public RaumListe(){
        super();
    }
    
    public void doImport(File selectedFile){
        try (BufferedReader br = new BufferedReader(new FileReader(selectedFile))) {
            clear();
                String line;
                while ((line = br.readLine()) != null) {
                if (!line.trim().contains("Raum")){      
                    Raum ll=new Raum(line.trim());
                    addElement(ll);
                }
            }
        }   catch (FileNotFoundException ex) {
                Logger.getLogger(LehrerListe.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(LehrerListe.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    
    
    public void doImport() {
        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        int returnValue = jfc.showOpenDialog(null);
	if (returnValue == JFileChooser.APPROVE_OPTION) {
            File selectedFile = jfc.getSelectedFile();
            System.out.println(selectedFile.getAbsolutePath());
            doImport(selectedFile);
        }
    }    
    
    
    public Raum get(String s){
        if (s.length()==0) return null;
        for (int i=0;i<this.size();i++){
            if (this.get(i).toString().equals(s)) return this.get(i);
        }
        Raum raum= new Raum(s);
        this.addElement(raum);
        return raum;
    }
}
