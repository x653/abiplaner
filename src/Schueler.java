/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.awt.Color;
import java.util.ArrayList;

public class Schueler {
    private final String vorname;
    private final String nachname;
    private ArrayList<Pruefung> abi4;
    private boolean select;
    private Color color;
    
    
    public Schueler(String v, String n){
        vorname=v;
        nachname=n;
        abi4=new ArrayList<>();
        select=false;
        color=null;
    }
    
    public void select(boolean a){
        select=a;
        checkKollision();
    }
    
    public void checkKollision(){
        System.out.println("check: "+this.toString()+abi4.size());
        for (int i=0;i<abi4.size();i++){
            abi4.get(i).update(false,select);
        }
        for (int i=0;i<abi4.size();i++){
            Pruefung p1=abi4.get(i);
            for (int j=i+1;j<abi4.size();j++){
                Pruefung p2=abi4.get(j);
                    if (p1.hasKollision(p2)){
                        p1.update(true,select);
                        p2.update(true,select);
                    }
            }
        }
    }
    
    public void setAbi4(Pruefung pAbi4){
        abi4.add(pAbi4);
        System.out.println(this.toString() + " Pruefung "+pAbi4.toString());
        if ((abi4.size()>1)&&(color==null)){
            color=ColorSeries.getNext();
        }
        System.out.println(color);

    }
    
    public String getVorname(){
        return vorname;
    }
    public String getNachname(){
        return nachname;
    }
    
    @Override
    public String toString(){
        return nachname+","+vorname;
    }
    public Color getColor(){
        return color;
    }
    
}
