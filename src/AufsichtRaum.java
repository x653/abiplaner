/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.JLabel;

public final class AufsichtRaum implements Bunt{
    private Raum raum;
    private final JLabel label;
    private final SlotAufsicht block;

    public AufsichtRaum(SlotAufsicht b){
        block = b;
        label=new JLabel("");
        label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        label.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        //addActionListener();
        raum=null;
    }
    public Tag getTag(){return block.getTag();}
    public boolean hasKollision(Bunt p){
        return (this.getTag()==p.getTag());
    }
    
    public boolean isEmpty(){
        if (raum!=null) return false;
        return true;
    }
    public void addActionListener(){
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                System.out.println("VorbRaum");
                action();
            }
        });
    }

    public JLabel getView(){
        return label;
    }
    public void setRaum(Raum r){
        if (raum==r) {
            r.removeEinsatz(this);
            raum=null;
        } else {
            if (raum!=null)
                raum.removeEinsatz(this);
            raum=r;
            raum.addEinsatz(this);
        }
        update();
    }
    
        public void update(boolean arg,boolean select) {
        if (arg){
            label.setOpaque(true);
            label.setBackground(Color.RED);
        } else if (select){
            label.setOpaque(true);
            label.setBackground(Color.GREEN);
        }else{ 
            label.setOpaque(false);
            label.setBackground(Color.white);
        }
    }

    
    
    private void action(){
        Clipboard clip=Clipboard.getClip();
        if (clip.hasRaum()) {
            setRaum(clip.pasteRaum());
            update();
        }
    }
    
    private void update(){
        label.setText(toString());
        raum.checkKollision();
    }
    
    @Override
    public String toString(){
        if (raum==null) return "";
        return raum.toString();
    }

}
