/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractListModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;


public class FileIO {
    
    public static void save(Model model,File pfad){
        save(model.getFaecher(),pfad+"/faecher.txt");
        model.getLehrer().filter("ALLE");
        save(model.getLehrer(),pfad+"/lehrer.txt");
        save(model.getRaeume(),pfad+"/raeume.txt");
        model.getSchueler().filter("ALLE");
        save(model.getSchueler(),pfad+"/schueler.txt");
        save(model.getTage(),pfad+"/tage.txt");
    }
        
    public static void save(AbstractListModel m,String file){
        System.out.println("serializing "+file);
        try {
          FileOutputStream fout = new FileOutputStream(file);
          PrintWriter p=new PrintWriter(fout);
            for (int i=0;i<m.getSize();i++){
                p.println(m.getElementAt(i));
            }
          p.close();
          }
       catch (Exception e) { e.printStackTrace(); }
    }
    
    public static void save(SchuelerListe m,String file){
        System.out.println("serializing "+file);
        try {
          FileOutputStream fout = new FileOutputStream(file);
          PrintWriter p=new PrintWriter(fout);
            for (int i=0;i<m.getSize();i++){
                        Pruefung pruef=m.elementAt(i);
                        if (pruef==null) p.print("null");
                        else {p.print(pruef.toString()+" [");
                            
                            for (int ll=0;ll<pruef.getEinsaetze().length;ll++){
                                p.print(pruef.getEinsaetze()[ll].toString());
                                if (ll<pruef.getEinsaetze().length-1) p.print(",");
                            }
                            p.print("]");
                        }
                        p.println();

            }
          p.close();
          }
       catch (Exception e) { e.printStackTrace(); }
    }


    public static void save(ArrayList<Tag> m,String file){
        System.out.println("serializing "+file);
        try {
          FileOutputStream fout = new FileOutputStream(file);
          PrintWriter p=new PrintWriter(fout);
            for (int i=0;i<m.size();i++){
                Tag tag=m.get(i);
                p.println("Tag: "+tag.toString());
                ArrayList<BlockPruefung> bloecke=tag.getBloecke();
                for (int j=0;j<bloecke.size();j++){
                    BlockPruefung pb=bloecke.get(j);
                    p.println("Block: "+pb.getRaum().toString());
                    SlotPruefung[] slot = pb.getPruefungen();
                    for (int k=0;k<slot.length;k++){
                        Pruefung pruef=slot[k].getPruefung();
                        if (pruef==null) p.print("null");
                        else {p.print("Pruefung: "+pruef.toString()+" [");
                            
                            for (int ll=0;ll<pruef.getEinsaetze().length;ll++){
                                p.print(pruef.getEinsaetze()[ll].toString());
                                if (ll<pruef.getEinsaetze().length-1) p.print(",");
                            }
                            p.print("]");
                        }
                        p.println();
                    }
                }
                ArrayList<SlotAufsicht> auf=tag.getAufsichtBloecke();
                for (int j=0;j<auf.size();j++){
                    p.print("Aufsicht: ");
                    p.print(auf.get(j).getName()+",");
                    p.print(auf.get(j).getRaum()+",");
                    p.print(auf.get(j).getAufsichtLehrer(0)+",");
                    p.println(auf.get(j).getAufsichtLehrer(1));

         
                }
            }
          p.close();
          }
       catch (Exception e) { e.printStackTrace(); }
    }

    public static Model load(File pfad){
        Model model=new Model();
        loadFaecher(model.getFaecher(),pfad+"/faecher.txt");
        loadRaeume(model.getRaeume(),pfad+"/raeume.txt");
        loadLehrer(model.getLehrer(),pfad+"/lehrer.txt");
        loadSchueler(model.getSchueler(),model.getLehrer(),pfad+"/schueler.txt");
        loadTage(model,pfad+"/tage.txt");
        return model;
    }        

    public static void loadFaecher(DefaultComboBoxModel<String> faecher,String file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                faecher.addElement(line);
            }

        }   catch (FileNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void loadLehrer(LehrerListe lehrer,String file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                String kuerzel=line.substring(0,line.indexOf(" "));
                Lehrer l=new Lehrer(kuerzel,"","");
                String[] faecher=line.substring(line.indexOf("(")+1,line.indexOf(")")).split(",");
                for (String fach:faecher) l.addFach(fach);
                lehrer.addElement(l);
            }

        }   catch (FileNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void loadRaeume(RaumListe raeume,String file) {
        
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                raeume.addElement(new Raum(line));
            }

        }   catch (FileNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void loadSchueler(SchuelerListe schueler,LehrerListe lehrer,String file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
                String fach=line.substring(0,line.indexOf("("));
                String kuerzel=line.substring(line.indexOf("(")+1,line.indexOf(")"));
                Lehrer l=lehrer.finde(kuerzel);
                String name=line.substring(line.indexOf("-")+1,line.indexOf(","));
                String vorname=line.substring(line.indexOf(",")+1,line.indexOf(" ["));
                Schueler s=schueler.finde(vorname,name);
                Pruefung p=new Pruefung(s,fach);
                p.setKurslehrer(l);
                schueler.addElement(p);
            }

        }   catch (FileNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void loadTage(Model model,String file) {
        
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            Tag tag=null;
            BlockPruefung block=null;
            Pruefung pruefung;
            int nummer=0;
            
            
            while ((line = br.readLine()) != null) {
                if (line.startsWith("Tag:")){
                    String stringtag=line.substring(5,line.length()-3).trim();
                    tag = new Tag(stringtag,(line.charAt(line.length()-2)=='V'));
                    model.addTag(tag);
                    System.out.println(stringtag);
                } else if (line.startsWith("Block:")){
                    String raum=line.substring(7);
                    block = new BlockPruefung(tag);
                    
                    Raum r=model.getRaeume().get(raum);
                    block.getRaum().setRaum(r);
                    tag.addBlockPruefung(block);
                    nummer=0;
                } else if (line.startsWith("Pruefung:")){
                    String pruef=line.substring(10);
                    String fach=pruef.substring(0,pruef.indexOf('('));
                    String lehrer=pruef.substring(pruef.indexOf('(')+1,pruef.indexOf(')'));
                    String[] schueler=pruef.substring(pruef.indexOf('-')+1,pruef.indexOf('[')).split(",",-1);
                    Schueler s=model.getSchueler().finde(schueler[1].trim(),schueler[0].trim());
                    pruefung=new Pruefung(s,fach);
                    pruefung.setKurslehrer(model.getLehrer().finde(lehrer));
   
                    block.getPruefungen()[nummer].setPruefung(pruefung);
                    nummer++;
                    String[] kommission=pruef.substring(pruef.indexOf('[')+1, pruef.indexOf(']')).split(",",-1);
                    pruefung.getEinsaetze()[0].setLehrer(model.getLehrer().finde(kommission[0]));
                    if (kommission[1].contains("(")) kommission[1]=kommission[1].substring(0,kommission[1].indexOf("("));                    if (model.getLehrer().finde(kommission[1])!=pruefung.getKurslehrer())
                        pruefung.getEinsaetze()[1].setLehrer(model.getLehrer().finde(kommission[1]));
                    pruefung.getEinsaetze()[2].setLehrer(model.getLehrer().finde(kommission[2]));
                }else if (line.startsWith("null")){
                    nummer++;
                }else if (line.startsWith("Aufsicht:")){
                    String[] aufsichten=line.substring(10).split(",",-1);
                    SlotAufsicht auf=new SlotAufsicht(tag,aufsichten[0]);
                    tag.addBlockAufsicht(auf);
                    Raum raum=model.getRaeume().get(aufsichten[1]);
                    auf.getAufsichtRaum().setRaum(raum);
                    String[] lehrer=aufsichten[2].split("/");
                    for (String l:lehrer){
                        Lehrer ll=model.getLehrer().finde(l);
                        if (ll!=null)
                            auf.getAufsichtLehrer(0).setLehrer(ll);
                    }
                    lehrer=aufsichten[3].split("/");
                    for (String l:lehrer){
                        Lehrer ll=model.getLehrer().finde(l);
                        if (ll!=null)
                            auf.getAufsichtLehrer(1).setLehrer(ll);
                    }
                    
                }
                
                
                
                
                
            }

        }   catch (FileNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}