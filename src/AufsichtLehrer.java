/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.JLabel;

public final class AufsichtLehrer implements Bunt{
    private final ArrayList<Lehrer> lehrer;
    private final JLabel label;
    private final SlotAufsicht block;
    private final boolean vor;

    public AufsichtLehrer(SlotAufsicht b,boolean vor){
        this.vor=vor;
        block = b;
        label=new JLabel("");
        label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        label.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        //addActionListener();
        lehrer=new ArrayList<>();
    }
    public boolean isEmpty(){
        if (lehrer.size()!=0) return false;
        return true;
    }
    public void addActionListener(){
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                System.out.println("VorbRaum");
                action();
            }
        });
    }
    public boolean isVor(){
        return vor;
    }

    public JLabel getView(){
        return label;
    }
    public void setLehrer(Lehrer l){
        if (!((Collection<Lehrer>)lehrer).contains(l)){
            lehrer.add(l);
            l.addPruefung(this);
        } else {
            lehrer.remove(l);
            l.removePruefung(this);
        }
        update();
    }
    
    private void action(){
        Clipboard clip=Clipboard.getClip();
        if (clip.hasLehrer()) {
            setLehrer(clip.getLehrer());
            update();
            clip.pasteLehrer();
        }
    }
    
    private void update(){
        label.setText(toString());
    }
    
    @Override
    public String toString(){
        StringBuilder s=new StringBuilder();
        for (int i=0;i<lehrer.size();i++){
            s.append(lehrer.get(i).getKuerzel());
            if (i<lehrer.size()-1) s.append("/");
        }
        return s.toString();
    }

    public void update(boolean arg,boolean select) {
        if (arg){
            label.setOpaque(true);
            label.setBackground(Color.RED);
        } else if (select){
            label.setOpaque(true);
            label.setBackground(Color.GREEN);
        }else{ 
            label.setOpaque(false);
            label.setBackground(Color.white);
        }
    }
    
    @Override
    public Tag getTag() {
        return block.getTag();
    }
    
    @Override
    public boolean hasKollision(Bunt p) {
        if (p.getClass()==this.getClass()){
            AufsichtLehrer p2=(AufsichtLehrer)p;
            if ((p2.block!=this.block)&&(p2.vor==this.vor)) return true;
        } else if (p.getClass()==Einsatz.class){
            Einsatz p2=(Einsatz)p;
            return (p2.hasKollision(this));
        }
        return false;
    }

}
