/**
 Abiplaner - erstelle geile Prüfungspläne.
 Copyright (C) 2019 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.filechooser.FileSystemView;


public class Model {
    private final LehrerListe lehrer;
    private final RaumListe raeume;
    private final SchuelerListe schueler;
    private final ArrayList<Tag> tage;
    private final FaecherListe faecher;

    public Model(){
        tage = new ArrayList<>();
        lehrer = new LehrerListe();
        raeume= new RaumListe();
        schueler = new SchuelerListe();
        faecher=new FaecherListe();
    }
    
    public LehrerListe getLehrer(){
        return lehrer;
    }
    
    public RaumListe getRaeume(){
        return raeume;
    }
    public SchuelerListe getSchueler(){
        return schueler;
    }
    public FaecherListe getFaecher(){
        return faecher;
    }
    public ArrayList<Tag> getTage(){
        return tage;
    }
    public void exportierePDF(File pfad,String datei){
        
        try {
            LaTeX.doExport(this,new File(pfad+"/export.tex"));
            List<String> commands = new ArrayList<String>();
            String os=System.getProperty("os.name");
            if (os.startsWith("Linux"))
                commands.add("pdflatex");
            else if (os.startsWith("Windows"))
                commands.add("pdflatex");
            else commands.add("/Library/TeX/texbin/pdflatex");
            
            //Add arguments
            commands.add(datei+".tex");
            System.out.println(commands);
            //Run macro on target
            ProcessBuilder pb = new ProcessBuilder(commands);
            pb.directory(pfad);
            pb.redirectErrorStream(false);  //WTF??
            Process process = pb.start();
            //Read output
            StringBuilder out = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = null, previous = null;
            while ((line = br.readLine()) != null)
                if (!line.equals(previous)) {
                    previous = line;
                    out.append(line).append('\n');
                    System.out.println(line);
                }
            //Check result
            if (process.waitFor() == 0) {
                System.out.println("Success!");
            }
            
            commands = new ArrayList<String>();
            if (os.startsWith("Linux"))
                commands.add("evince");
            else if (os.startsWith("Windows"))
                commands.add("Explorer");
            else commands.add("/usr/bin/open");
                
            //Add arguments
            commands.add(datei+".pdf");
            System.out.println(commands);
            //Run macro on target
            pb = new ProcessBuilder(commands);
            pb.directory(pfad);
            pb.redirectErrorStream(false);//WTF??
             process = pb.start();
            //Read output
             out = new StringBuilder();
             br = new BufferedReader(new InputStreamReader(process.getInputStream()));
             line = null;
             previous = null;
            while ((line = br.readLine()) != null)
                if (!line.equals(previous)) {
                    previous = line;
                    out.append(line).append('\n');
                    System.out.println(line);
                }
            //Check result
            if (process.waitFor() == 0) {
                System.out.println("Success!");
            }
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void filter(){
        System.out.println("Filter auf: "+faecher.getSelectedItem());
        schueler.filter((String)faecher.getSelectedItem());
        lehrer.filter((String)faecher.getSelectedItem());
    }
    public void addBlockA(int i){
        if (i>=0 && i<tage.size())
        tage.get(i).neuerBlockAufsicht();  
    }
    
    public void addBlock(int i){
        if (i>=0 && i<tage.size())
        tage.get(i).neuerBlockPruefung();
    }
    
    public void addRaum(){
        String raum= JOptionPane.showInputDialog("Neuer Raum:");
        raeume.addElement(new Raum(raum));
    }
    public void addLoL(){
        String lehrerIn= JOptionPane.showInputDialog("Neuer Lehrer:\n<Kürzel>,<Fach1>,<Fach2>,...");
        String[] lin=lehrerIn.split(",");
        Lehrer l=new Lehrer(lin[0],"","");
        for (int i=1;i<lin.length;i++){
                l.addFach(lin[i]);
        }
        lehrer.addElement(l);  
    }
    public void addSoS(){
        String lehrerIn= JOptionPane.showInputDialog("Neuer Schüler:\n<Vorname>,<Nachname>,<Fach>,<Lehrer>");
        String[] lin=lehrerIn.split(",");
        if (lin.length!=4) System.out.println("Kommas?");
        else {
            Schueler s=schueler.finde(lin[0],lin[1]);
            Pruefung p= new Pruefung(s,lin[2]);
            Lehrer l=lehrer.finde(lin[3]);
            if (l==null) {
                System.out.println("lehrer nicht vohanden: "+lin[3]);
                l=new Lehrer(lin[3],"no","name");
                lehrer.addElement(l);                
            }
            else p.setKurslehrer(l);
            schueler.addElement(p);
        }
          
    }
    
    public void addTag(JTabbedPane t){
        String tag= JOptionPane.showInputDialog("Neuer Tag:");
        Tag tv=new Tag(tag,true);
        tage.add(tv);
        t.add(new JScrollPane(tv.getPanel()),tv.toString());
        Tag tn=new Tag(tag,false);
        tage.add(tn);
        t.add(new JScrollPane(tn.getPanel()),tn.toString());
        tv.addActionListener();
        tn.addActionListener();
    }
 
    public void addTag(Tag tv){
        tage.add(tv);
    }
 
    public void loescheLeereBloecke(int i){
        tage.get(i).deleteLeereBlocke();
        tage.get(i).deleteLeereBlockeAufsicht();
    }
    
    public void doImport(File datei){
        if (datei==null) datei=new java.io.File(".");
        JFileChooser jfc = new JFileChooser(datei);
        int returnValue = jfc.showOpenDialog(null);
	if (returnValue == JFileChooser.APPROVE_OPTION) {
            File f = jfc.getSelectedFile();
            System.out.println(f.getAbsolutePath());
            faecher.addElement("ALLE");
        
       try (BufferedReader br = new BufferedReader(new FileReader(f))) {
                schueler.clear();
                String line;
                String kurs="";
                String fach="";
                String einlehrer="";
                while ((line = br.readLine()) != null) {
                    if (line.contains("Lehrkraft:")){
                        String[] lll=line.split("Lehrkraft:");
                        einlehrer=lll[1].trim();
                        kurs=lll[0].trim();
                        fach=(kurs.split("-"))[0];
                    }
                    else {
                        if (line.contains(",")){
                            String[] lll=line.split(",");
                            Schueler s=schueler.finde(lll[1].trim(),lll[0].trim());
              
                            Pruefung p=new Pruefung(s,fach);
                            
                            faecher.addElement(fach);
                            System.out.println(lehrer.finde(einlehrer.trim()));
                            if (lehrer.finde(einlehrer.trim())!=null){
                                p.setKurslehrer(lehrer.finde(einlehrer.trim()));
                            }else {
                                if (lehrer.finde("NN")==null){
                                    lehrer.addElement(new Lehrer("NN","no","name"));
                                }
                                p.setKurslehrer(lehrer.finde("NN"));

                                 System.out.println("Achtung Prüfung ohne Lehrer!");
                            }
                            schueler.addElement(p);
                        }
                        
                    }
                }
                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SchuelerListe.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SchuelerListe.class.getName()).log(Level.SEVERE, null, ex);
            }    }
   }

      public void doImport3(File datei){
          if (datei==null) datei=new java.io.File(".");
        JFileChooser jfc = new JFileChooser(datei);
        int returnValue = jfc.showOpenDialog(null);
	if (returnValue == JFileChooser.APPROVE_OPTION) {
            File f = jfc.getSelectedFile();
            System.out.println(f.getAbsolutePath());
            faecher.addElement("ALLE");
                try (BufferedReader br = new BufferedReader(new FileReader(f))) {
                schueler.clear();
                String line;
                while ((line = br.readLine()) != null) {
                    String[] lll=line.split(";");
                    if (lll.length>1){
                        String[] sch=lll[0].split(",");
                        if (sch.length==2){
                            Schueler s=schueler.finde(sch[1].trim(),sch[0].trim());
                            
                            System.out.println(s.toString());
                            for (int ii=1;ii<lll.length;ii++){
                                String[] ppp=lll[ii].trim().split(",");
                             
                                if (ppp.length==2){
                                    String fach=ppp[0].trim();
                                    String derlehrer=ppp[1].trim();
                                    Pruefung p=new Pruefung(s,fach);
                            faecher.addElement(fach);
                            System.out.println(lehrer.finde(derlehrer.trim()));
                            if (lehrer.finde(derlehrer.trim())!=null){
                                p.setKurslehrer(lehrer.finde(derlehrer.trim()));
                            }else {
                                if (lehrer.finde("NN")==null){
                                    lehrer.addElement(new Lehrer("NN","no","name"));
                                }
                                p.setKurslehrer(lehrer.finde("NN"));

                                 System.out.println("Achtung Prüfung ohne Lehrer!");
                            }
                            schueler.addElement(p);
                        }
                                    
                                }
                            }
                        }
                    }
                        
                
                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SchuelerListe.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SchuelerListe.class.getName()).log(Level.SEVERE, null, ex);
            }
   }}

   
    
}
