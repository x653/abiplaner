# Abiplaner

JAVA app um schnell geile Abiprüfungspläne zu erzeugen.

## Feature
* Datenimport aus SchILD möglich (Lehrer/Schüler/Prüfungen)
* erzeugt geile pdf (nur, wenn LaTeX installiert ist)
* verwaltet Anzahl an Einsätze je Lehrer
* entdeckt Kollisionen

## Voraussetzungen:
* JAVA
* LaTeX (pdflatex)